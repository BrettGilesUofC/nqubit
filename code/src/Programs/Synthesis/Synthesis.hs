{-# LANGUAGE FlexibleInstances, UndecidableInstances, GADTs,
KindSignatures, TypeFamilies, TypeOperators, FlexibleContexts,
ScopedTypeVariables, GeneralizedNewtypeDeriving,
MultiParamTypeClasses, FunctionalDependencies, IncoherentInstances #-}

{-# OPTIONS_GHC -fno-warn-incomplete-patterns #-}

-- | This module provides functions that are useful for experimenting
-- with exact and approximate synthesis of single-qubit unitaries.

module Programs.Synthesis.Synthesis where

import Data.Ratio
import Data.List
import Data.Complex
import Data.Bits
import System.Random

-- ----------------------------------------------------------------------
-- * General auxiliary

-- | If /n/≠0, return (/a/,/k/) such that /a/ is odd and /n/ =
-- /a/⋅2[sup /k/]. If /n/=0, return (/0/,/0/).
lobit :: Integer -> (Integer, Integer)
lobit 0 = (0,0)
lobit n = aux n where
  aux n
    | even n = (a,k+1) 
    | otherwise = (n,0)
      where 
        (a,k) = aux (n `div` 2)
        
-- | If /n/ is of the form 2[sup /k/], return /k/. Otherwise, return
-- 'Nothing'.
log2 :: Integer -> Maybe Integer
log2 n
  | a == 1 = Just k
  | otherwise = Nothing
    where
      (a,k) = lobit n

-- ----------------------------------------------------------------------
-- * Rational numbers

-- | We define our own variant of the rational numbers, which is an
-- identical copy of 'Rational', except that it has a more sensible
-- 'Show' instance.
newtype Rationals = ToRationals { fromRationals :: Rational }
                  deriving (Num, Eq, Ord)
                           
-- | An auxiliary function for printing rational numbers, using
-- correct precedences, and omitting denominators of 1.
showsPrec_rational :: Integral a => Int -> Ratio a -> ShowS
showsPrec_rational d a
  | denom == 1 = showsPrec d numer
  | numer < 0  = showParen (d > 5) $ showString "-" . showsPrec_rational 6 (-a)
  | otherwise  = showParen (d > 6) $
                 showsPrec 7 numer . showString "/" . showsPrec 7 denom
    where
      numer = numerator a
      denom = denominator a

instance Show Rationals where
  showsPrec d (ToRationals a) = showsPrec_rational d a

-- ----------------------------------------------------------------------
-- * Rings

-- | A type class to denote rings. We make 'Ring' a synonym of
-- Haskell's 'Num' type class, so that we can use the usual notation
-- '+', '-', '*' for the ring operations.  This is not a perfect fit,
-- because Haskell's 'Num' class also contains two non-ring operations
-- 'abs' and 'signum'.  By convention, we usually leave these
-- operations undefined (with error message) for unsigned rings.

class (Num a) => Ring a
instance (Num a) => Ring a

-- | A type class for rings that contain 1\/2.
class (Ring a) => HalfRing a where
  -- | The value 1\/2.
  half :: a
  -- | Conversion from 'Dyadic' to any ring containing 1\/2.
  fromDyadic :: Dyadic -> a
  fromDyadic (Dyadic a n) = fromInteger a * half^n

-- | A type class for rings that contain √2.
class (Ring a) => RootTwoRing a where
  -- | The square root of 2.
  roottwo :: a
  -- | Conversion from ℤ[√2] to any ring containing √2.
  fromDInteger :: DInteger -> a
  fromDInteger (RootTwo x y) = fromInteger x + roottwo * fromInteger y

-- | A type class for rings that contain 1\/√2.
class (HalfRing a, RootTwoRing a) => RootHalfRing a where
  -- | The square root of 1\/2.
  roothalf :: a
  -- | Conversion from ℤ[1\/√2] to any ring containing 1\/√2.
  fromDReal :: DReal -> a
  fromDReal (RootTwo x y) = fromDyadic x + roottwo * fromDyadic y

-- | A type class for rings that contain a square root of -1.
class (Ring a) => ComplexRing a where
  -- | The complex unit.
  i :: a

-- | A type class for rings with adjoints.
class Adjoint a where
  -- | Compute the adjoint (complex conjugate transpose).
  adj :: a -> a

instance Adjoint Integer where
  adj x = x
  
instance Adjoint Int where
  adj x = x
  
instance Adjoint Double where
  adj x = x
  
instance Adjoint Float where
  adj x = x
  
instance Adjoint Rationals where  
  adj x = x
  
instance RootHalfRing Double where
  roothalf = sqrt 0.5

instance RootTwoRing Double where
  roottwo = sqrt 2

instance HalfRing Double where
  half = 0.5

instance RootHalfRing Float where
  roothalf = sqrt 0.5

instance RootTwoRing Float where
  roottwo = sqrt 2

instance HalfRing Float where
  half = 0.5

instance HalfRing Rationals where
  half = ToRationals (1%2)

instance (Ring a, RealFloat a) => ComplexRing (Complex a) where
  i = 0 :+ 1

instance (RootHalfRing a, RealFloat a) => RootHalfRing (Complex a) where
  roothalf = roothalf :+ 0

instance (RootTwoRing a, RealFloat a) => RootTwoRing (Complex a) where
  roottwo = roottwo :+ 0

instance (HalfRing a, RealFloat a) => HalfRing (Complex a) where
  half = half :+ 0

-- | A type class for rings that contain a square root of /i/, or
-- equivalently, a fourth root of −1.
class (Ring a) => OmegaRing a where
  -- | The square root of /i/.
  omega :: a
  -- | Conversion from ℤ[ω] to any ring containing ω.
  fromZOmega :: ZOmega -> a
  fromZOmega (Omega a b c d) = fromInteger a * omega^3 + fromInteger b * omega^2 + fromInteger c * omega + fromInteger d

instance (ComplexRing a, RootHalfRing a) => OmegaRing a where
  omega = roothalf * (1 + i)

-- ----------------------------------------------------------------------
-- * The ring ℤ[1\/2] of dyadic rationals

-- | A dyadic rational is a rational number whose denominator is a
-- power of 2. 
-- 
-- We internally represent a dyadic rational /a/\/2[sup /n/] as a pair
-- (/a/,/n/). Note that this representation is not unique. When it is
-- necessary to choose a canonical representative, we choose the least
-- possible /n/≥0.
data Dyadic = Dyadic Integer Integer

instance Real Dyadic where
  toRational (Dyadic a n) 
    | n >= 0    = a % 2^n
    | otherwise = a * 2^(-n) % 1

instance Show Dyadic where
  showsPrec d a = showsPrec_rational d (toRational a)

instance Eq Dyadic where
  Dyadic a n == Dyadic b m = a * 2^(k-n) == b * 2^(k-m) where
    k = max n m

instance Ord Dyadic where
  compare (Dyadic a n) (Dyadic b m) = compare (a * 2^(k-n)) (b * 2^(k-m)) where
    k = max n m

instance Num Dyadic where
  Dyadic a n + Dyadic b m = Dyadic c k where
    k = max n m
    c = a * 2^(k-n) + b * 2^(k-m)
  Dyadic a n * Dyadic b m = Dyadic (a*b) (n+m)
  negate (Dyadic a n) = Dyadic (-a) n
  abs x = if x >= 0 then x else -x
  signum x = case compare 0 x of { LT -> 1; EQ -> 0; GT -> -1 }
  fromInteger n = Dyadic n 0

instance HalfRing Dyadic where
  half = Dyadic 1 1

instance Adjoint Dyadic where
  adj x = x

-- | Given a dyadic rational /r/, return (/a/,/n/) such that /r/ =
-- /a/\/2[sup /n/], where /n/≥0 is chosen as small as possible.
from_dyadic :: Dyadic -> (Integer, Integer)
from_dyadic (Dyadic a n) 
  | a == 0 = (0, 0)
  | n >= k = (b, n-k)
  | otherwise = (b*2^(k-n), 0)
  where
    (b,k) = lobit a

-- | Given a dyadic rational /r/ and an integer /k/≥0, such that /a/ =
-- /r/2[sup /k/] is an integer, return /a/. If /a/ is not an integer,
-- the behavior is undefined.
integer_of_dyadic :: Dyadic -> Integer -> Integer
integer_of_dyadic (Dyadic a n) k =
  shift a (fromInteger (k-n))

-- | An alternative constructor: equivalent to 'Dyadic', but also
-- normalizes the internal representation. 
-- 
-- Note: because our implementation of 'lobit' is not optimized,
-- intermediate normalization actually slows down computation, so we
-- do not normalize after each arithmetic operation.
nDyadic :: Integer -> Integer -> Dyadic
nDyadic b k = Dyadic a n where
  (a,n) = from_dyadic (Dyadic b k)

-- ----------------------------------------------------------------------
-- * The ring /R/[√2]
  
-- | The ring /R/[√2], where /R/ is any ring. The value 'RootTwo' /a/
-- /b/ represents /a/ + /b/ √2.
data RootTwo a = RootTwo a a
                deriving (Eq)

instance (Ring a) => RootTwoRing (RootTwo a) where
  roottwo = RootTwo 0 1

instance (HalfRing a) => RootHalfRing (RootTwo a) where
  roothalf = RootTwo 0 half
  
instance (HalfRing a) => HalfRing (RootTwo a) where
  half = RootTwo half 0
  
instance (Adjoint a) => Adjoint (RootTwo a) where  
  adj (RootTwo a b) = RootTwo (adj a) (adj b)

instance (Ring a) => Num (RootTwo a) where
  RootTwo a b + RootTwo a' b' = RootTwo a'' b'' where
    a'' = a + a'
    b'' = b + b'
  RootTwo a b * RootTwo a' b' = RootTwo a'' b'' where
    a'' = a * a' + 2 * b * b'
    b'' = a * b' + a' * b
  negate (RootTwo a b) = RootTwo a' b' where
    a' = -a
    b' = -b
  fromInteger n = RootTwo n' 0 where
    n' = fromInteger n
  abs f = f * signum f
  signum f@(RootTwo a b)
    | sa == 0 && sb == 0 = 0
    | sa /= -1 && sb /= -1 = 1
    | sa /= 1 && sb /= 1 = -1
    | sa /= -1 && sb /= 1 && signum (a*a - 2*b*b) /= -1 = 1
    | sa /= 1 && sb /= -1 && signum (a*a - 2*b*b) /= 1  = 1
    | otherwise = -1
    where
      sa = signum a
      sb = signum b

instance (HalfRing a) => Ord (RootTwo a) where
  x <= y  =  signum (y-x) /= (-1)
  
instance (Num a) => Show (RootTwo a) where
  showsPrec d (RootTwo a 0) = showsPrec d a
  showsPrec d (RootTwo 0 1) = showString "√2"
  showsPrec d (RootTwo 0 (-1)) = showParen (d > 5) $ showString "-√2"
  showsPrec d (RootTwo 0 b) = showParen (d > 10) $ 
    showsPrec 11 b . showString " √2"
  showsPrec d (RootTwo a b) | signum b == 1 = showParen (d > 6) $
    showsPrec 7 a . showString " + " . showsPrec 7 (RootTwo 0 b)
  showsPrec d (RootTwo a b) | otherwise = showParen (d > 6) $
    showsPrec 7 a . showString " - " . showsPrec 7 (RootTwo 0 (-b))

-- | The ring ℤ[1\/√2]. The \"D\" stands for \"dyadic\".
type DReal = RootTwo Dyadic

-- | The field ℚ[√2]. We think of the elements of ℚ[√2] as (certain)
-- exact real numbers, so the \"E\" stands for \"exact\".
type EReal = RootTwo Rationals

-- | The ring ℤ[√2].
type DInteger = RootTwo Integer

-- ----------------------------------------------------------------------
-- * Complex numbers

-- | Our own version of the complex numbers over some ring. The
-- problem with the Haskell libraries standard 'Complex' /a/ type is
-- that it assumes too much, for example, it assumes /a/ is a member
-- of the 'RealFloat' class. Also, this allows us to define a more
-- sensible 'Show' instance.
data Cplx a = Cplx a a
            deriving (Eq)
                     
instance (Num a) => Show (Cplx a) where
  showsPrec d (Cplx a 0) = showsPrec d a
  showsPrec d (Cplx 0 1) = showString "i"
  showsPrec d (Cplx 0 (-1)) = showParen (d > 5) $ showString "-i"
  showsPrec d (Cplx 0 b) = showParen (d > 10) $ 
    showsPrec 11 b . showString " i"
  showsPrec d (Cplx a b) | signum b == 1 = showParen (d > 6) $
    showsPrec 7 a . showString " + " . showsPrec 7 (Cplx 0 b)
  showsPrec d (Cplx a b) | otherwise = showParen (d > 6) $
    showsPrec 7 a . showString " - " . showsPrec 7 (Cplx 0 (-b))

instance (Num a) => Num (Cplx a) where
  Cplx a b + Cplx a' b' = Cplx a'' b'' where
    a'' = a + a'
    b'' = b + b'
  Cplx a b * Cplx a' b' = Cplx a'' b'' where
    a'' = a * a' - b * b'
    b'' = a * b' + a' * b
  negate (Cplx a b) = Cplx a' b' where
    a' = -a
    b' = -b
  fromInteger n = Cplx n' 0 where
    n' = fromInteger n
  abs f = error "abs undefined at Cplx"
  signum f = error "signum undefined at Cplx"

instance (RootHalfRing a) => RootHalfRing (Cplx a) where
  roothalf = Cplx roothalf 0

instance (RootTwoRing a) => RootTwoRing (Cplx a) where
  roottwo = Cplx roottwo 0

instance (HalfRing a) => HalfRing (Cplx a) where
  half = Cplx half 0

instance (Num a) => ComplexRing (Cplx a) where
  i = Cplx 0 1

instance (Adjoint a, Num a) => Adjoint (Cplx a) where
  adj (Cplx a b) = (Cplx (adj a) (-(adj b)))

-- | The field ℚ[√2, /i/]. We think of the elements as (certain) exact
-- complex numbers, hence the \"E\" in the name.
type EComplex = Cplx EReal

-- | The ring ℤ[1\/√2, /i/]. The \"D\" stands for \"dyadic\".
type DComplex = Cplx DReal

-- | Double precision complex floating point numbers.
type CDouble = Cplx Double

-- | Single precision complex floating point numbers.
type CFloat = Cplx Float

-- | Take the real part of a complex number.
real :: Cplx a -> a
real (Cplx a b) = a

-- ----------------------------------------------------------------------
-- * The ring /R/[ω]

-- | The ring /R/[ω], where /R/ is any ring. The value 'Omega' /a/ /b/
-- /c/ /d/ represents /a/ω[sup 3]+/b/ω[sup 2]+/c/ω+/d/.
data Omega a = Omega a a a a
            deriving (Eq)
                     
instance (Num a) => Show (Omega a) where
  show (Omega a b c d) = "Ω" ++ show (a,b,c,d)

instance (Num a) => Num (Omega a) where
  Omega a b c d + Omega a' b' c' d' = Omega a'' b'' c'' d'' where
    a'' = a + a'
    b'' = b + b'
    c'' = c + c'
    d'' = d + d'
  Omega a b c d * Omega a' b' c' d' = Omega a'' b'' c'' d'' where  
    a'' = a*d' + b*c' + c*b' + d*a'
    b'' = b*d' + c*c' + d*b' - a*a'
    c'' = c*d' + d*c' - a*b' - b*a'
    d'' = d*d' - a*c' - b*b' - c*a'
  negate (Omega a b c d) = Omega (-a) (-b) (-c) (-d) where
  fromInteger n = Omega 0 0 0 n' where
    n' = fromInteger n
  abs f = error "abs undefined at Omega"
  signum f = error "signum undefined at Omega"

instance (HalfRing a) => RootHalfRing (Omega a) where
  roothalf = Omega (-half) 0 half 0

instance (Num a) => RootTwoRing (Omega a) where
  roottwo = Omega (-1) 0 1 0

instance (HalfRing a) => HalfRing (Omega a) where
  half = Omega 0 0 0 half

instance (Num a) => ComplexRing (Omega a) where
  i = Omega 0 1 0 0

instance (Adjoint a, Num a) => Adjoint (Omega a) where
  adj (Omega a b c d) = Omega (-(adj c)) (-(adj b)) (-(adj a)) (adj d)

-- | The ring ℤ[ω].
type ZOmega = Omega Integer

-- This is an undecidable instance, but is not overlapping.
instance OmegaRing ZOmega where
  omega = Omega 0 0 1 0

-- This is an undecidable instance, but is not overlapping.
instance OmegaRing (Omega Z2) where
  omega = Omega 0 0 1 0

-- | The ring /D/[ω].
type DOmega = Omega Dyadic

-- This is an overlapping instance.
instance Show DOmega where
  showsPrec = showsPrec_DenomExp
  
-- This is an overlapping instance.
instance Show (Matrix n m DOmega) where
  showsPrec = showsPrec_DenomExp
  
-- This is an overlapping instance.
instance Show (Matrix n m DComplex) where
  showsPrec = showsPrec_DenomExp

-- This is an overlapping instance.
instance Show DComplex where
  showsPrec = showsPrec_DenomExp

-- ----------------------------------------------------------------------
-- * The ring ℤ[sub 2]

-- | The ring ℤ[sub 2] of integers modulo 2. 
data Z2 = Even | Odd
        deriving (Eq)
                     
instance Show Z2 where
  show Even = "0"
  show Odd = "1"

instance Num Z2 where
  Even + x = x
  x + Even = x
  Odd + Odd = Even
  Even * x = Even
  x * Even = Even
  Odd * Odd = Odd
  negate x = x
  fromInteger n = if even n then Even else Odd
  abs f = f
  signum f = f

instance Adjoint Z2 where
  adj x = x

-- ----------------------------------------------------------------------
-- * Conversion to dyadic

-- | A type class relating \"rational\" types to their dyadic
-- counterparts.
class ToDyadic a b | a -> b where
  -- | Convert a \"rational\" value to a \"dyadic\" value, if the
  -- denominator is a power of 2. Otherwise, return 'Nothing'.
  toDyadic :: a -> Maybe b

instance ToDyadic Dyadic Dyadic where
  toDyadic = return

instance ToDyadic Rational Dyadic where
  toDyadic x = do
    k <- log2 denom
    return (Dyadic numer k)
    where denom = denominator x
          numer = numerator x

instance ToDyadic Rationals Dyadic where
  toDyadic = toDyadic . fromRationals

instance (ToDyadic a b) => ToDyadic (RootTwo a) (RootTwo b) where
  toDyadic (RootTwo x y) = do
    x' <- toDyadic x
    y' <- toDyadic y
    return (RootTwo x' y')

instance (ToDyadic a b) => ToDyadic (Cplx a) (Cplx b) where
  toDyadic (Cplx x y) = do
    x' <- toDyadic x
    y' <- toDyadic y
    return (Cplx x' y')

-- ----------------------------------------------------------------------
-- * Parity
    
-- | A type class for things that have parity.
class Parity a where
  -- | Return the parity of something.
  parity :: a -> Z2

instance Integral a => Parity a where
  parity x = if even x then 0 else 1
  
instance Parity DInteger where
  parity (RootTwo a b) = parity a

-- ----------------------------------------------------------------------
-- * Residues

-- | A type class for things that have residues. In a typical
-- instance, /a/ is a ring whose elements are expressed with
-- coefficients in ℤ, and /b/ is a corresponding ring whose elements
-- are expressed with coefficients in ℤ[sub 2].
class Residue a b | a -> b where
  -- | Return the residue of something.
  residue :: a -> b
  
instance Residue Integer Z2 where
  residue = parity
  
instance Residue a b => Residue (Omega a) (Omega b) where
  residue (Omega a b c d) = Omega (residue a) (residue b) (residue c) (residue d)

instance Residue a b => Residue (RootTwo a) (RootTwo b) where
  residue (RootTwo a b) = RootTwo (residue a) (residue b)
  
instance (Residue a a', Residue b b') => Residue (a,b) (a',b') where
  residue (x,y) = (residue x, residue y)
  
instance Residue () () where  
  residue = const ()
  
instance (Residue a b) => Residue [a] [b] where  
  residue = map residue
  
instance (Residue a b) => Residue (Cplx a) (Cplx b) where  
  residue (Cplx a b) = Cplx (residue a) (residue b)
  
instance (Residue a b) => Residue (Vector n a) (Vector n b) where  
  residue = vector_map residue
  
instance (Residue a b) => Residue (Matrix m n a) (Matrix m n b) where
  residue (Matrix m) = Matrix (residue m)
  
-- ----------------------------------------------------------------------
-- * Type-level natural numbers
  
-- $ Note: with Haskell 7.4.2 DataKinds, this could be replaced by a
-- tigher definition; however, the following works just fine in
-- Haskell 7.2.

-- | Type-level representation of zero.
data Zero

-- | Type-level representation of successor.
data Succ a

-- | The natural number 1 as a type.
type One = Succ Zero

-- | The natural number 2 as a type.
type Two = Succ One

-- | The natural number 3 as a type.
type Three = Succ Two

-- | The natural number 4 as a type.
type Four = Succ Three

-- | The natural number 5 as a type.
type Five = Succ Four

-- | The natural number 6 as a type.
type Six = Succ Five

-- | The natural number 7 as a type.
type Seven = Succ Six

-- | The natural number 8 as a type.
type Eight = Succ Seven

-- | The natural number 9 as a type.
type Nine = Succ Eight

-- | The natural number 10 as a type.
type Ten = Succ Nine



-- | A data type for the natural numbers. @NNat@ /n/ is non-empty iff
-- /n/ is a type-level natural number; moreover, it has unique element
-- 'IsZero' if /n/=0, and 'IsSucc' if /n/>0.
data NNat :: * -> * where
  IsZero :: NNat Zero
  IsSucc :: (Nat n) => NNat (Succ n)

-- | A type class for the natural numbers. The members are exactly the
-- type-level natural numbers.
class Nat n where
  -- | Return 'IsZero' or 'IsSucc', depending on whether /n/='Zero' or not.
  nat :: NNat n
instance Nat Zero where
  nat = IsZero
instance (Nat a) => Nat (Succ a) where
  nat = IsSucc

-- | Addition of type-level natural numbers.
type family Plus n m
type instance Zero `Plus` m = m
type instance (Succ n) `Plus` m = Succ (n `Plus` m)

-- | Multiplication of type-level natural numbers.
type family Times n m
type instance Zero `Times` m = Zero
type instance (Succ n) `Times` m = m `Plus` (n `Times` m)

-- ----------------------------------------------------------------------
-- * Fixed-length vectors

-- | @Vector /n/ /a/@ is the type of lists of length /n/ with elements
-- from /a/. We call this a \"vector\" rather than a tuple or list for
-- two reasons: the vectors are homogeneous (all elements have the
-- same type), and they are strict: if any one component is undefined,
-- the whole vector is undefined.
data Vector :: * -> * -> * where
  Nil :: Vector Zero a
  Cons :: !a -> !(Vector n a) -> Vector (Succ n) a

infixr 5 `Cons`

-- | Construct a vector of length 1.
vector_singleton :: a -> Vector One a
vector_singleton x = x `Cons` Nil

-- | Convert a fixed-length list to an ordinary list.
list_of_vector :: Vector n a -> [a]
list_of_vector Nil = []
list_of_vector (Cons h t) = h : list_of_vector t

instance (Show a) => Show (Vector n a) where
  show = show . list_of_vector

instance (Eq a) => Eq (Vector n a) where
  Nil == Nil = True
  Cons a as == Cons b bs = a == b && as == bs

-- | Zip two equal length lists.
vector_zipwith :: (a -> b -> c) -> Vector n a -> Vector n b -> Vector n c
vector_zipwith f Nil Nil = Nil
vector_zipwith f (Cons a as) (Cons b bs) = Cons c cs where
  c = f a b
  cs = vector_zipwith f as bs

-- | Map a function over a fixed-length list.
vector_map :: (a -> b) -> Vector n a -> Vector n b
vector_map f Nil = Nil
vector_map f (Cons a as) = Cons (f a) (vector_map f as)

-- | Create the vector (0, 1, …, /n/-1).
vector_enum :: (Num a, Nat n) => Vector n a
vector_enum = aux nat 0 where
  aux :: (Num a) => NNat n -> a -> Vector n a
  aux IsZero a = Nil
  aux IsSucc a = Cons a (aux nat (a+1))

-- | Create the vector (/f/(0), /f/(1), …, /f/(/n/-1)).
vector_of_function :: (Num a, Nat n) => (a -> b) -> Vector n b
vector_of_function f = vector_map f vector_enum

-- | Return a fixed-length list consisting of a repetition of the
-- given element. Unlike 'replicate', no count is needed, because this
-- information is already contained in the type. However, the type
-- must of course be inferrable from the context.
vector_repeat :: (Nat n) => a -> Vector n a
vector_repeat x = vector_of_function (const x)

-- | Turn a list of columns into a list of rows.
vector_transpose :: (Nat m) => Vector n (Vector m a) -> Vector m (Vector n a)
vector_transpose Nil = vector_repeat Nil
vector_transpose (Cons a as) = vector_zipwith Cons a (vector_transpose as)

-- | Left strict fold over a fixed-length list.
vector_foldl :: (a -> b -> a) -> a -> Vector n b -> a
vector_foldl f x l = foldl f x (list_of_vector l)

-- | Right fold over a fixed-length list.
vector_foldr :: (a -> b -> b) -> b -> Vector n a -> b
vector_foldr f x l = foldr f x (list_of_vector l)

-- | Return the tail of a fixed-length list.
vector_tail :: Vector (Succ n) a -> Vector n a
vector_tail (Cons h t) = t

-- | Return the head of a fixed-length list.
vector_head :: Vector (Succ n) a -> a
vector_head (Cons h t) = h

-- | Append two fixed-length lists.
vector_append :: Vector n a -> Vector m a -> Vector (n `Plus` m) a
vector_append Nil v = v
vector_append (Cons h t) v = Cons h (vector_append t v)

-- | Version of 'sequence' for fixed-length lists.
vector_sequence :: (Monad m) => Vector n (m a) -> m (Vector n a)
vector_sequence Nil = return Nil
vector_sequence (Cons a as) = do
  a' <- a
  as' <- vector_sequence as
  return (Cons a' as')

instance (ToDyadic a b) => ToDyadic (Vector n a) (Vector n b) where
  toDyadic as = vector_sequence (vector_map toDyadic as)

-- ----------------------------------------------------------------------
-- * /m/×/n/-Matrices

-- | An /m/×/n/-matrix is a list of /n/ columns, each of which is a
-- list of /m/ scalars.
data Matrix m n a = Matrix (Vector n (Vector m a))
               deriving (Eq, Show)

-- | Decompose a matrix into a list of columns.
unMatrix :: Matrix m n a -> (Vector n (Vector m a))
unMatrix (Matrix m) = m

-- | Addition of /m/×/n/-matrices. We use a special symbol because
-- /m/×/n/-matrices do not form a ring; only /n/×/n/-matrices form a
-- ring (in which case the normal symbol '+' also works).
(.+.) :: (Num a) => Matrix m n a -> Matrix m n a -> Matrix m n a
Matrix a .+. Matrix b = Matrix c where
  c = vector_zipwith (vector_zipwith (+)) a b

-- | Map some function over every element of a matrix.
matrix_map :: (a -> b) -> Matrix m n a -> Matrix m n b
matrix_map f (Matrix a) = Matrix b where
  b = vector_map (vector_map f) a

-- | Create the matrix whose /i/,/j/-entry is (/i/,/j/). Here /i/ and
-- /j/ are 0-based, i.e., the top left entry is (0,0).
matrix_enum :: (Num a, Nat n, Nat m) => Matrix m n (a,a)
matrix_enum = Matrix (vector_of_function f) where
  f i = vector_of_function (\j -> (j,i))

-- | Create the matrix whose /i/,/j/-entry is @f i j@. Here /i/ and
-- /j/ are 0-based, i.e., the top left entry is @f 0 0@.
matrix_of_function :: (Num a, Nat n, Nat m) => (a -> a -> b) -> Matrix m n b
matrix_of_function f = matrix_map (uncurry f) matrix_enum

-- | Multiplication of a scalar and an /m/×/n/-matrix.
scalarmult :: (Num a) => a -> Matrix m n a -> Matrix m n a
scalarmult x m = matrix_map (x *) m

-- | Multiplication of /m/×/n/-matrices. We use a special symbol
-- because /m/×/n/-matrices do not form a ring; only /n/×/n/-matrices
-- form a ring (in which case the normal symbol '*' also works).
(.*.) :: (Num a, Nat m) => Matrix m n a -> Matrix n p a -> Matrix m p a
Matrix a .*. Matrix b = Matrix c where
  c = vector_map (a `mmv`) b
  
  mmv :: (Num a, Nat m) => Vector n (Vector m a) -> Vector n a -> Vector m a
  Nil `mmv` Nil = vector_repeat 0
  (Cons h t) `mmv` (Cons k s) = (k `msv` h) `avv` (t `mmv` s)
  
  msv :: (Num b) => b -> Vector n b -> Vector n b
  k `msv` h = vector_map (k*) h
  
  avv :: (Num c) => Vector n c -> Vector n c -> Vector n c
  v `avv` w = vector_zipwith (+) v w

-- | Return the 0 matrix of the given dimension.
null_matrix :: (Num a, Nat n, Nat m) => Matrix m n a
null_matrix = Matrix (vector_repeat (vector_repeat 0))

-- | Take the adjoint of an /m/×/n/-matrix. Unlike 'adj', this can be
-- applied to non-square matrices.
adjoint :: (Nat m, Adjoint a) => Matrix m n a -> Matrix n m a
adjoint (Matrix a) = Matrix c where
  b = vector_map (vector_map adj) a
  c = vector_transpose b
  
-- | Return a list of all the entries of a matrix, in some fixed but
-- unspecified order.
matrix_entries :: Matrix m n a -> [a]
matrix_entries (Matrix m) = 
  concat $ map list_of_vector $ list_of_vector m

-- | Version of 'sequence' for matrices.
matrix_sequence :: (Monad m) => Matrix n p (m a) -> m (Matrix n p a)
matrix_sequence (Matrix m) = do
  m' <- vector_sequence (vector_map vector_sequence m)
  return (Matrix m')


instance (ToDyadic a b) => ToDyadic (Matrix m n a) (Matrix m n b) where
  toDyadic (Matrix a) = do
    b <- toDyadic a
    return (Matrix b)

-- ----------------------------------------------------------------------
-- * The ring of square matrices

instance (Num a, Nat n) => Num (Matrix n n a) where
  (+) = (.+.)
  (*) = (.*.)
  negate = scalarmult (-1)
  fromInteger x = matrix_of_function (\i j -> if i == j then fromInteger x else 0)
  abs a = error "abs: undefined for Matrix"
  signum a = error "signum: undefined for Matrix"
        
instance (Nat n, Adjoint a) => Adjoint (Matrix n n a) where
  adj (Matrix a) = Matrix c where
    b = vector_map (vector_map adj) a
    c = vector_transpose b

instance (HalfRing a, Nat n) => HalfRing (Matrix n n a) where
  half = scalarmult half 1

instance (RootHalfRing a, Nat n) => RootHalfRing (Matrix n n a) where
  roothalf = scalarmult roothalf 1

instance (RootTwoRing a, Nat n) => RootTwoRing (Matrix n n a) where
  roottwo = scalarmult roottwo 1

instance (ComplexRing a, Nat n) => ComplexRing (Matrix n n a) where
  i = scalarmult i 1

-- | Return the trace of a square matrix.
tr :: (Ring a) => Matrix n n a -> a
tr (Matrix a) = aux a where
  aux :: (Num a) => Vector n (Vector n a) -> a
  aux Nil = 0
  aux ((h `Cons` t) `Cons` s) = h + aux (vector_map vector_tail s)

-- | Return the square of the Hilbert-Schmidt norm of an
-- /m/×/n/-matrix, defined by ‖/M/‖² = tr /M/[sup †]/M/.
hs_sqnorm :: (Ring a, Adjoint a, Nat n) => Matrix n m a -> a
hs_sqnorm m = tr (m .*. adjoint m)

-- | A convenience constructor for 2×2-matrices. The arguments are by
-- rows.
matrix2x2 :: (a, a) -> (a, a) -> Matrix Two Two a
matrix2x2 (a, b) (c, d) = Matrix ((a `Cons` c `Cons` Nil) `Cons` (b `Cons` d `Cons` Nil) `Cons` Nil)

-- | A convenience constructor for 3×3-matrices. The arguments are by
-- rows.
matrix3x3 :: (a, a, a) -> (a, a, a) -> (a, a, a) -> Matrix Three Three a
matrix3x3 (a0, a1, a2) (b0, b1, b2) (c0, c1, c2) = 
  Matrix ((a0 `Cons` b0 `Cons` c0 `Cons` Nil) 
          `Cons` (a1 `Cons` b1 `Cons` c1 `Cons` Nil) 
          `Cons` (a2 `Cons` b2 `Cons` c2 `Cons` Nil) 
          `Cons` Nil)

-- | A convenience constructor for 4×4-matrices. The arguments are by
-- rows.
matrix4x4 :: (a, a, a, a) -> (a, a, a, a) -> (a, a, a, a) -> (a, a, a, a) -> Matrix Four Four a
matrix4x4 (a0, a1, a2, a3) (b0, b1, b2, b3) (c0, c1, c2, c3) (d0, d1, d2, d3) = 
  Matrix ((a0 `Cons` b0 `Cons` c0 `Cons` d0 `Cons` Nil) 
          `Cons` (a1 `Cons` b1 `Cons` c1 `Cons` d1 `Cons` Nil) 
          `Cons` (a2 `Cons` b2 `Cons` c2 `Cons` d2 `Cons` Nil) 
          `Cons` (a3 `Cons` b3 `Cons` c3 `Cons` d3 `Cons` Nil) 
          `Cons` Nil)

-- | A convenience constructor for 3-dimensional column vectors.
column3 :: (a, a, a) -> Matrix Three One a
column3 (a, b, c) = column_matrix (a `Cons` b `Cons` c `Cons` Nil)

-- | A convenience destructor for 3-dimensional column vectors. This
-- is the inverse of 'column3'.
from_column3 :: Matrix Three One a -> (a, a, a)
from_column3 (Matrix ((a `Cons` b `Cons` c `Cons` Nil) `Cons` Nil)) = (a, b, c)

-- | A convenience constructor for turning a vector into a column matrix.
column_matrix :: Vector n a -> Matrix n One a
column_matrix v = Matrix (vector_singleton v)

-- ----------------------------------------------------------------------
-- * Tensor

-- | Stack matrices vertically.
stack_vertical :: Matrix m n a -> Matrix p n a -> Matrix (m `Plus` p) n a
stack_vertical (Matrix a) (Matrix b) = (Matrix c) where
  c = vector_zipwith vector_append a b

-- | Stack matrices horizontally.
stack_horizontal :: Matrix m n a -> Matrix m p a -> Matrix m (n `Plus` p) a
stack_horizontal (Matrix a) (Matrix b) = (Matrix c) where
  c = vector_append a b
  
-- | Repeat a matrix vertically, according to some vector of scalars.
tensor_vertical :: (Num a, Nat n) => Vector p a -> Matrix m n a -> Matrix (p `Times` m) n a
tensor_vertical v m = concat_vertical (vector_map (`scalarmult` m) v)
                               
-- | Vertically concatenate a vector of matrices.
concat_vertical :: (Num a, Nat n) => Vector p (Matrix m n a) -> Matrix (p `Times` m) n a
concat_vertical Nil = null_matrix
concat_vertical (Cons h t) = stack_vertical h (concat_vertical t)

-- | Repeat a matrix horizontally, according to some vector of scalars.
tensor_horizontal :: (Num a, Nat m) => Vector p a -> Matrix m n a -> Matrix m (p `Times` n) a
tensor_horizontal v m = concat_horizontal (vector_map (`scalarmult` m) v)
  
-- | Horizontally concatenate a vector of matrices.
concat_horizontal :: (Num a, Nat m) => Vector p (Matrix m n a) -> Matrix m (p `Times` n) a
concat_horizontal Nil = null_matrix
concat_horizontal (Cons h t) = stack_horizontal h (concat_horizontal t)

-- | Kronecker tensor of two matrices.
tensor :: (Num a, Nat n, Nat (p `Times` m)) => Matrix p q a -> Matrix m n a -> Matrix (p `Times` m) (q `Times` n) a
tensor a b = ab3 where
  Matrix ab1 = matrix_map (`scalarmult` b) a
  ab2 = vector_map concat_vertical ab1
  ab3 = concat_horizontal ab2

-- | Form a diagonal block matrix.
oplus :: (Num a, Nat m, Nat q, Nat n, Nat p) => Matrix p q a -> Matrix m n a -> Matrix (p `Plus` m) (q `Plus` n) a
oplus (a :: Matrix p q a) (b :: Matrix m n a) = 
  (a `stack_vertical` (null_matrix :: Matrix m q a)) `stack_horizontal` ((null_matrix :: Matrix p n a) `stack_vertical` b)

-- | A controlled gate.
controlled :: (Num a, Nat n) => Matrix n n a -> Matrix (n `Plus` n) (n `Plus` n) a
controlled (m :: Matrix n n a) = oplus (1 :: Matrix n n a) m

-- | Controlled-not gate.
cnot :: (Num a) => Matrix Four Four a
cnot = controlled u2_X

-- | Swap gate.
swap :: (Num a) => Matrix Four Four a
swap = matrix4x4 (1,0,0,0)
                 (0,0,1,0)
                 (0,1,0,0)
                 (0,0,0,1)

-- ----------------------------------------------------------------------
-- * Interchange formats

-- $ It is convenient to have a simple but exact \"interchange
-- format\" for operators in the single-qubit Clifford+/T/ group.  Our
-- format is simply a list of gates from /X/, /Y/, /Z/, /H/, /S/, /T/,
-- with the obvious interpretation as a matrix product. We also
-- include the global phase gate /W/ = [exp /i/π\/4], although it is
-- not fully implemented. Different operator representations used by
-- various parts of this software can be converted to and from this
-- format.

-- | An enumeration type to represent symbolic basic gates (/X/, /Y/,
-- /Z/, /H/, /S/, /T/, /W/).
data Gate = X | Y | Z | H | S | T | W
          deriving Show

-- | A type class for all things that can be exactly converted to a
-- list of gates. These are the exact representations of the
-- Clifford+/T/ group.
class Gatelist a where
  -- | Convert any suitable thing to a list of gates.
  to_gates :: a -> [Gate]
  
instance Gatelist Gate where
  to_gates x = [x]
    
instance (Gatelist a) => Gatelist [a] where
  to_gates x = concat [ to_gates y | y <- x ]
  
instance Gatelist Char where
  to_gates 'X' = [X]
  to_gates 'Y' = [Y]
  to_gates 'Z' = [Z]
  to_gates 'H' = [H]
  to_gates 'S' = [S]
  to_gates 'T' = [T]
  to_gates 'W' = [W]
  to_gates x = error $ "to_gates[Char]: undefined -- " ++ (show x)

-- | A type class for all things that a list of gates can be converted
-- to.
class FromGates a where
  -- | Convert a list of gates to any suitable type.
  from_gates :: [Gate] -> a

instance FromGates String where
  from_gates = concat . map show

instance FromGates [Gate] where
  from_gates = id

-- | Invert a gate list.
invert_gates :: [Gate] -> [Gate]
invert_gates gs = aux [] gs where
  aux acc [] = acc
  aux acc (X:t) = aux (X:acc) t
  aux acc (H:t) = aux (H:acc) t
  aux acc (S:t) = aux (S:S:S:acc) t
  aux acc (T:t) = aux (T:S:S:S:acc) t

-- | Convert any precise format to any format.
convert :: (Gatelist a, FromGates b) => a -> b
convert = from_gates . to_gates

-- ----------------------------------------------------------------------
-- * Clifford operators

-- | Return a representation of the /k/th Clifford operator, where /k/ ∈ {0, …, 23}.
clifford :: (FromGates a) => Int -> a
clifford 0 = from_gates []
clifford 1 = from_gates [H]
clifford 2 = from_gates [H,S,S,H]
clifford 3 = from_gates [S,S]
clifford 4 = from_gates [S]
clifford 5 = from_gates [S,S,S]
clifford 6 = from_gates [H,S,S]
clifford 7 = from_gates [S,S,H]
clifford 8 = from_gates [S,H]
clifford 9 = from_gates [S,S,S,H]
clifford 10 = from_gates [S,S,H,S,S,H]
clifford 11 = from_gates [S,H,S,S,H]
clifford 12 = from_gates [S,S,S,H,S,S,H]
clifford 13 = from_gates [H,S]
clifford 14 = from_gates [H,S,S,S]
clifford 15 = from_gates [S,S,H,S,S]
clifford 16 = from_gates [S,H,S,S]
clifford 17 = from_gates [S,S,S,H,S,S]
clifford 18 = from_gates [H,S,H]
clifford 19 = from_gates [H,S,S,S,H]
clifford 20 = from_gates [H,S,H,S,S,H]
clifford 21 = from_gates [H,S,S,S,H,S,S,H]
clifford 22 = from_gates [S,S,S,H,S]
clifford 23 = from_gates [S,H,S,S,S]
clifford x = error $ "clifford: illegal argument" ++ show x

-- $ Symbolic representation of the Clifford group.

-- | An axis: /I/, /H/, or /SH/. 
data Axis = R_I | R_H | R_SH
          deriving (Show, Eq)

-- | A flip: /I/ or /X/.
data Flip = F_I | F_X
          deriving Show

-- | A turn: /I/, /S/, /SS/, /SSS/.
data Turn = T_I | T_S | T_SS | T_SSS
          deriving Show

-- | Each of the 24 Clifford gates (modulo global phase) can be
-- uniquely specified as a turn followed by a flip and an axis. 
type Clifford = (Axis, Flip, Turn)

instance Gatelist Axis where  
  to_gates R_I = []
  to_gates R_H = [H]
  to_gates R_SH = [S, H]
  
instance Gatelist Flip where
  to_gates F_I = []
  to_gates F_X = [X]

instance Gatelist Turn where
  to_gates T_I = []
  to_gates T_S = [S]
  to_gates T_SS = [S, S]
  to_gates T_SSS = [S, S, S]
  
instance Gatelist Clifford where
  to_gates (x,y,z) = to_gates x ++ to_gates y ++ to_gates z

-- | The identity Clifford gate.
clifford_id :: Clifford
clifford_id = (R_I, F_I, T_I)

-- | Convert a matrix to a Clifford gate. If the matrix isn't
-- Clifford, the behavior is undefined.
-- 
-- Note: it would be cleaner (and much faster) to do this
-- symbolically.  This function is only used internally, so the
-- precondition should always hold.
clifford_of_matrix :: SO3 DReal -> Clifford
clifford_of_matrix m = (axis,flip,turn)
  where
    (a,b,c) = from_column3 (m .*. column3 (0,0,1))
    axis = if c /= 0 then R_I else if a /= 0 then R_H else R_SH
    flip = if a+b+c == (-1) then F_X else F_I
    m' = convert (axis,flip,T_I)
    m'' = adj m' * m
    (x,y,z) = from_column3 (m'' .*. column3 (1,0,0))
    turn = case (x,y,z) of
      (1,0,0) -> T_I
      (0,1,0) -> T_S
      (-1,0,0) -> T_SS
      (0,-1,0) -> T_SSS
      _ -> T_I
           
-- | Multiply two gates and convert to a Clifford operator. The
-- behavior is undefined if the operands are not Clifford gates.
-- 
-- This function is only used internally by 'normalform_append', so the
-- precondition should always hold.
clifford_mult :: (Gatelist a, Gatelist b) => a -> b -> Clifford
clifford_mult a b = clifford_of_matrix ((convert a) * (convert b))

-- ----------------------------------------------------------------------
-- * Matrices in /U/(2)

-- | A convenient abbreviation for the type of 2×2-matrices.
type U2 a = Matrix Two Two a

-- | The Pauli /X/ operator.
u2_X :: (Ring a) => U2 a
u2_X = matrix2x2 (0, 1) 
                 (1, 0)

-- | The Pauli /Y/ operator.
u2_Y :: (ComplexRing a) => U2 a
u2_Y = matrix2x2 (0, -i) 
                 (i,  0)

-- | The Pauli /Z/ operator.
u2_Z :: (Ring a) => U2 a
u2_Z = matrix2x2 (1,  0) 
                 (0, -1)

-- | The Hadamard operator.
u2_H :: (RootHalfRing a) => U2 a
u2_H = roothalf * matrix2x2 (1,  1) 
                            (1, -1)

-- | The /S/ operator.
u2_S :: (ComplexRing a) => U2 a
u2_S = matrix2x2 (1, 0) 
                 (0, i)

-- | The /T/ operator.
u2_T :: (RootHalfRing a, ComplexRing a) => U2 a
u2_T = matrix2x2 (1,     0) 
                 (0, omega)

-- | The /W/ = [exp /i/π\/4] global phase operator.
u2_W :: (RootHalfRing a, ComplexRing a) => U2 a
u2_W = omega

-- | Convert a symbolic gate to the corresponding operator.
u2_of_gate :: (RootHalfRing a, ComplexRing a) => Gate -> U2 a
u2_of_gate X = u2_X
u2_of_gate Y = u2_Y
u2_of_gate Z = u2_Z
u2_of_gate H = u2_H
u2_of_gate S = u2_S
u2_of_gate T = u2_T
u2_of_gate W = u2_W

instance (RootHalfRing a, ComplexRing a) => FromGates (U2 a) where
  from_gates = product . map u2_of_gate

-- ----------------------------------------------------------------------
-- * Matrices in /SO/(3).

-- $ This is the Bloch sphere representation of single qubit
-- operators.

-- | A convenient abbreviation for the type of 3×3-matrices.
type SO3 a = Matrix Three Three a

-- | The Pauli /X/ operator.
so3_X :: (Ring a) => SO3 a
so3_X = matrix3x3 (1,  0,  0) 
                  (0, -1,  0) 
                  (0,  0, -1)

-- | The Pauli /Y/ operator.
so3_Y :: (Ring a) => SO3 a
so3_Y = matrix3x3 (-1, 0,  0) 
                  ( 0, 1,  0) 
                  ( 0, 0, -1)

-- | The Pauli /Z/ operator.
so3_Z :: (Ring a) => SO3 a
so3_Z = matrix3x3 (-1,  0, 0) 
                  ( 0, -1, 0) 
                  ( 0,  0, 1)

-- | The Hadamard operator.
so3_H :: (Ring a) => SO3 a
so3_H = matrix3x3 (0,  0, 1) 
                  (0, -1, 0) 
                  (1,  0, 0)

-- | The /S/ operator.
so3_S :: (Ring a) => SO3 a
so3_S = matrix3x3 (0, -1, 0) 
                  (1,  0, 0) 
                  (0,  0, 1)

-- | The /T/ operator.
so3_T :: (RootHalfRing a) => SO3 a
so3_T = matrix3x3 (r, -r,  0)
                  (r,  r,  0)
                  (0,  0,  1)
  where r = roothalf

-- | Convert a symbolic gate to the corresponding Bloch sphere
-- operator.
so3_of_gate :: (RootHalfRing a) => Gate -> SO3 a
so3_of_gate X = so3_X
so3_of_gate Y = so3_Y
so3_of_gate Z = so3_Z
so3_of_gate H = so3_H
so3_of_gate S = so3_S
so3_of_gate T = so3_T
so3_of_gate W = 1

instance (RootHalfRing a) => FromGates (SO3 a) where
  from_gates = product . map so3_of_gate

-- | Conversion from /U/(2) to /SO/(3).
so3_of_u2 :: (HalfRing a, Adjoint a) => U2 (Cplx a) -> SO3 a
so3_of_u2 u = matrix_of_function f where
  f i j = half * (real $ tr (sigma i * u * sigma j * adj u))
  sigma 0 = u2_X
  sigma 1 = u2_Y
  sigma 2 = u2_Z

-- ----------------------------------------------------------------------
-- * Matsumoto-Amano normal forms
  
-- ----------------------------------------------------------------------
-- ** Representation of normal forms
  
-- | A representation of normal forms, optimized for right
-- multiplication. 
data NormalForm = NormalForm Syllables Clifford

-- | Syllables is a circuit of the form (/I/|/T/)(/HT/|/SHT/)[sup *].
data Syllables = S_I | S_T | SApp Syllables CAxis
            deriving (Show)

-- | An axis change: /H/ or /SH/.
data CAxis = N_HT | N_SHT
           deriving (Show)

instance Gatelist NormalForm where
  to_gates (NormalForm ts c) = to_gates ts ++ to_gates c
  
instance Gatelist Syllables where
  to_gates S_I = []
  to_gates S_T = [T]
  to_gates (SApp ts axis) = to_gates ts ++ to_gates axis

instance Gatelist CAxis where
  to_gates N_HT = [H, T]
  to_gates N_SHT = [S, H, T]

instance Show NormalForm where
  show x = case to_gates x of
    [] -> "ε"
    gs -> concat $ map show gs    

-- | Right-multiply the given normal form by a gate.
normalform_append :: NormalForm -> Gate -> NormalForm
normalform_append (NormalForm ts c) X = NormalForm ts (c `clifford_mult` X)
normalform_append (NormalForm ts c) Y = NormalForm ts (c `clifford_mult` Y)
normalform_append (NormalForm ts c) Z = NormalForm ts (c `clifford_mult` Z)
normalform_append (NormalForm ts c) H = NormalForm ts (c `clifford_mult` H)
normalform_append (NormalForm ts c) S = NormalForm ts (c `clifford_mult` S)
normalform_append (NormalForm ts c) T
  | a == R_H =
    NormalForm (SApp ts N_HT) c'
  | a == R_SH =
    NormalForm (SApp ts N_SHT) c'
  | otherwise = case ts of
      S_I -> NormalForm S_T c'
      S_T -> NormalForm S_I ("S" `clifford_mult` c')
      SApp tss N_HT -> NormalForm tss ("HS" `clifford_mult` c')
      SApp tss N_SHT -> NormalForm tss ("SHS" `clifford_mult` c')
  where
    -- cT = aTc'
    (a,f,t) = c
    c' = (R_I, f, t')
    t' = case (f,t) of
      (F_I,t) -> t
      (F_X, T_I) -> T_S
      (F_X, T_S) -> T_SS
      (F_X, T_SS) -> T_SSS
      (F_X, T_SSS) -> T_I

-- ----------------------------------------------------------------------
-- ** Group operations on normal forms

-- | The identity as a normal form.
nf_id :: NormalForm
nf_id = NormalForm S_I clifford_id

-- | Invert a normal form. The input can be any 'Gatelist'.
invert :: (Gatelist a) => a -> NormalForm
invert = from_gates . invert_gates . to_gates

-- | Multiply two normal forms. The right factor can be any
-- 'Gatelist'.
times :: (Gatelist b) => NormalForm -> b -> NormalForm
times a b = foldl' normalform_append a (to_gates b)

-- ----------------------------------------------------------------------
-- ** Conversion to normal form

-- | Convert any 'Gatelist' list to a 'NormalForm', thereby normalizing it.
normalize :: (Gatelist a) => a -> NormalForm
normalize = times nf_id

instance FromGates NormalForm where
  from_gates = normalize

-- ----------------------------------------------------------------------
-- ** Alternative representation of normal forms

-- $ A representation of normal forms that is optimized for
-- enumeration.  This version is reversed (inside-out) relative to
-- 'NormalForm'.

-- | An element of the Clifford + /T/ group whose normal form does not
-- end in /T/. It consists of a clifford circuit, followed by 0 or more
-- gates of the form /HT/ or /SHT/.
data CliffordT = Clifford Clifford | App CAxis CliffordT
               deriving (Show)

-- | An element of the Clifford + /T/ group in normal form. This
-- consists of a 'CliffordT', followed by an optional /T/.
data AltNF = NF_T CliffordT | NF CliffordT

instance Gatelist CliffordT where
  to_gates (Clifford c) = to_gates c
  to_gates (App x y) = to_gates x ++ to_gates y
  
instance Gatelist AltNF where
  to_gates (NF_T x) = T : to_gates x
  to_gates (NF x) = to_gates x

instance Show AltNF where
  show x = case to_gates x of
    [] -> "ε"
    gs -> concat $ map show gs    

-- | Compute the /T/-depth of a normal form.
nf_depth :: AltNF -> Int
nf_depth (NF_T x) = 1 + ct_depth x
nf_depth (NF x) = ct_depth x

-- | Compute the /T/-depth of a normal form not ending in /T/.
ct_depth :: CliffordT -> Int
ct_depth (Clifford x) = 0
ct_depth (App x y) = 1 + ct_depth y

-- | Convert a 'NormalForm' to an 'AltNF'.
normalform_to_altnf :: NormalForm -> AltNF
normalform_to_altnf (NormalForm ts c) = aux ts (Clifford c) where 
  aux S_I ct = NF ct
  aux S_T ct = NF_T ct
  aux (SApp t rot) ct = aux t (App rot ct)

instance FromGates AltNF where
  from_gates = normalform_to_altnf . from_gates

-- ----------------------------------------------------------------------
-- * Rings of integers
  
-- | A type class for rings that have a distinguished subring \"of
-- integers\". A typical instance is 'DReal', which has 'DInteger' as
-- its ring of integers.
class WholePart a b | a -> b where  
  -- | The subring injection of /b/ into /a/.
  from_whole :: b -> a
  -- | The inverse of 'from_whole'. Throws an error if the given
  -- element is not actually in /b/.
  to_whole :: a -> b
  
instance WholePart Dyadic Integer where
  from_whole = fromInteger
  to_whole d 
    | n == 0 = a
    | otherwise = error "to_whole: non-integral value"
    where
      (a,n) = from_dyadic d

instance WholePart DReal DInteger where
  from_whole = fromDInteger
  to_whole (RootTwo x y) = RootTwo (to_whole x) (to_whole y)
  
instance WholePart DOmega ZOmega where
  from_whole = fromZOmega
  to_whole (Omega x y z w) = Omega (to_whole x) (to_whole y) (to_whole z) (to_whole w)
  
instance (WholePart a a', WholePart b b') => WholePart (a,b) (a',b') where
  from_whole (x,y) = (from_whole x, from_whole y)
  to_whole (x,y) = (to_whole x, to_whole y)
  
instance WholePart () () where  
  from_whole = const ()
  to_whole = const ()
  
instance (WholePart a b) => WholePart [a] [b] where  
  from_whole = map from_whole
  to_whole = map to_whole
  
instance (WholePart a b) => WholePart (Cplx a) (Cplx b) where  
  from_whole (Cplx a b) = Cplx (from_whole a) (from_whole b)
  to_whole (Cplx a b) = Cplx (to_whole a) (to_whole b)
  
instance (WholePart a b) => WholePart (Vector n a) (Vector n b) where  
  from_whole = vector_map from_whole
  to_whole = vector_map to_whole
  
instance (WholePart a b) => WholePart (Matrix m n a) (Matrix m n b) where
  from_whole (Matrix m) = Matrix (from_whole m)
  to_whole (Matrix m) = Matrix (to_whole m)
  
-- ----------------------------------------------------------------------
-- * Common denominators
  
-- | A type class for things from which a common power of 1\/√2 (a
-- least denominator exponent) can be factored out. Typical instances
-- are 'DReal', 'DComplex', as well as tuples, lists, vectors, and
-- matrices thereof. Here /a/ is the type that has denominator
-- exponents, and /b/ is the corresponding \"integer\" type.
class DenomExp a where
  -- | Calculate the least denominator exponent /k/ of /a/. Returns
  -- the smallest /k/≥0 such that /a/ = /b/\/√2[sup /k/] for some
  -- integral /b/.
  denomexp :: a -> Integer
  
  -- | Factor out a /k/th power of 1\/√2 from /a/. In other words,
  -- calculate /a/√2[sup /k/].
  denomexp_factor :: a -> Integer -> a

-- | Calculate and factor out the least denominator exponent /k/ of
-- /a/. Return (/b/,/k/), where /a/ = /b/\/(√2)[sup /k/] and /k/≥0.
denomexp_decompose :: (WholePart a b, DenomExp a) => a -> (b, Integer)
denomexp_decompose a = (b, k) where
  k = denomexp a
  b = to_whole (denomexp_factor a k)

-- | Generic show-like method that factors out a common denominator
-- exponent.
showsPrec_DenomExp :: (WholePart a b, Show b, DenomExp a) => Int -> a -> ShowS
showsPrec_DenomExp d a 
  | k == 0 = showsPrec d b
  | k == 1 = showParen (d > 7) $ 
             showString "1/√2 " . showsPrec 7 b
  | otherwise = showParen (d > 7) $
                showString ("1/√2^" ++ show k ++ " ") . showsPrec 7 b
  where (b, k) = denomexp_decompose a

instance DenomExp DReal where
  denomexp (RootTwo x y) = k'
    where
      (a,k) = from_dyadic x
      (b,l) = from_dyadic y
      k' = maximum [2*k, 2*l-1]
  denomexp_factor a k = a * roottwo^k

instance DenomExp DOmega where
  denomexp (Omega x y z w) = k'
      where
        (a,ak) = from_dyadic x
        (b,bk) = from_dyadic y
        (c,ck) = from_dyadic z
        (d,dk) = from_dyadic w
        k = maximum [ak, bk, ck, dk]
        a' = if k == ak then a else 0
        b' = if k == bk then b else 0
        c' = if k == ck then c else 0
        d' = if k == dk then d else 0
        k' | k>0 && even (a'-c') && even (b'-d') = 2*k-1
           | otherwise = 2*k
  denomexp_factor a k = a * roottwo^k
        
instance (DenomExp a, DenomExp b) => DenomExp (a,b) where
  denomexp (a,b) = denomexp a `max` denomexp b
  denomexp_factor (a,b) k = (denomexp_factor a k, denomexp_factor b k)

instance DenomExp () where
  denomexp _ = 0
  denomexp_factor _ k = ()

instance (DenomExp a) => DenomExp [a] where
  denomexp as = maximum (0 : [ denomexp a | a <- as ])
  denomexp_factor as k = [ denomexp_factor a k | a <- as ]

instance (DenomExp a) => DenomExp (Cplx a) where
  denomexp (Cplx a b) = denomexp a `max` denomexp b
  denomexp_factor (Cplx a b) k = Cplx (denomexp_factor a k) (denomexp_factor b k)

instance (DenomExp a) => DenomExp (Vector n a) where
  denomexp as = denomexp (list_of_vector as)
  denomexp_factor as k = vector_map (\a -> denomexp_factor a k) as
  
instance (DenomExp a) => DenomExp (Matrix m n a) where
  denomexp (Matrix m) = denomexp m
  denomexp_factor (Matrix m) k = Matrix (denomexp_factor m k)

-- ----------------------------------------------------------------------
-- * Exact synthesis
  
-- | Input an exact matrix in /SO/(3), and output the corresponding
-- Clifford+/T/ normal form. It is an error if the given matrix is not
-- an element of /SO/(3), i.e., orthogonal with determinant 1.

-- ### Todo: update aux m 0 to check whether the matrix was actually
-- Clifford. Currently, the conversion happily succeeds on matrices of
-- determinant −1.
synthesis_bloch :: SO3 DReal -> [Gate]
synthesis_bloch m = aux m1 k
  where
    (m1, k) = denomexp_decompose m
    aux m 0 = to_gates (clifford_of_matrix (matrix_map fromDInteger m))
    aux m k = to_gates axis ++ [T] ++ aux m5 (k-1)
      where
        Matrix p = matrix_map parity m
        v1 = vector_head p
        v2 = vector_head (vector_tail p)
        v = list_of_vector $ vector_zipwith (\x y -> x + y - x*y) v1 v2
        axis = case v of
          [1, 1, 0] -> R_I
          [0, 1, 1] -> R_H
          [1, 0, 1] -> R_SH
          _ -> error "synthesis_bloch: not unitary"
        m2 = axischange axis * m
        m3 = adj sqrt2T * m2
        m4 = matrix_sequence $ matrix_map half_DInteger m3
        m5 = case m4 of 
          Nothing -> error "synthesis_bloch: not unitary"
          Just m -> m
    sqrt2T = matrix3x3 (1, -1, 0) (1, 1, 0) (0, 0, roottwo)
    axischange R_I = 1
    axischange R_H = adj so3_H
    axischange R_SH = adj (so3_S * so3_H)

instance (ToDyadic a DReal, Gatelist (SO3 DReal)) => Gatelist (SO3 a) where
  to_gates m = case toDyadic m of
    Nothing -> error "to_gates: coefficients not in ℤ[1/√2]"
    Just m1 -> synthesis_bloch m1

instance (ToDyadic a DComplex, Gatelist (U2 DComplex)) => Gatelist (U2 a) where
  to_gates m = case toDyadic m of
    Nothing -> error "to_gates: coefficients not in ℤ[1/√2]"
    Just m1 -> synthesis_bloch (so3_of_u2 m1)

-- ### Todo: normalize in Dyadic arithmetic, because this speeds up
-- computation (note: EReal is faster than DReal!)

-- | Divide a 'DInteger' of the form 2/a/ + 2/b/√2 by 2, or return
-- 'Nothing' if it is not of the required form.
half_DInteger :: DInteger -> Maybe DInteger
half_DInteger (RootTwo a b)
  | even a && even b = Just (RootTwo a' b')
  | otherwise = Nothing
    where
      a' = a `div` 2
      b' = b `div` 2

-- ### Todo: For optimization: note that symbolic normalization takes
-- about 4–5 times longer, and uses 2–3 times more memory, than
-- converting to and from a matrix. This is ridiculous, as the
-- computational effort should be trivial. It may be that the
-- bottleneck is our slow implementation of Clifford operations and
-- the Clifford-T commutation (CT -> TC | HTC | SHTC). Note: the
-- computational time seems to be linear, so there is no direct
-- complexity sink. But each individual multiplication seems to be
-- exceedingly slow (only about 160 gates per second, compared to 500
-- gates per second when going via synthesis in @SO3 EReal@).

-- ----------------------------------------------------------------------
-- * Multi-qubit synthesis

-- ----------------------------------------------------------------------
-- ** Symbolic representation of one- and two-level gates

-- | An index for a row or column of a matrix.
type Index = Int

-- | Symbolic representation of one- and two-level gates.
data TwoLevel = 
  TL_X Index Index -- ^ /X/[sub /i/,/j/]
  | TL_H Index Index -- ^ /H/[sub /i/,/j/]
  | TL_T Integer Index Index -- ^ /T/[super /k/][sub /i/,/j/]
  | TL_omega Integer Index -- ^ ω[super /k/][sub /i/]
  deriving (Show)

-- | Construct a two-level matrix.
twolevel :: (Ring a, Nat n) => (a,a) -> (a,a) -> Index -> Index -> Matrix n n a
twolevel (a,b) (c,d) i j = matrix_of_function f where
  f x y 
    | x == i && y == i = a
    | x == i && y == j = b
    | x == j && y == i = c
    | x == j && y == j = d
    | x == y = 1
    | otherwise = 0
                  
-- | Construct a one-level matrix.                  
onelevel :: (Ring a, Nat n) => a -> Index -> Matrix n n a
onelevel a i = matrix_of_function f where
  f x y
    | x == i && y == i = a
    | x == y = 1
    | otherwise = 0

-- | Convert a symbolic one- or two-level gate into an operator.
matrix_of_twolevel :: (ComplexRing a, RootHalfRing a, Nat n) => TwoLevel -> Matrix n n a
matrix_of_twolevel (TL_X i j) = twolevel (0,1) (1,0) i j
matrix_of_twolevel (TL_H i j) = twolevel (s,s) (s,-s) i j
  where s = roothalf
matrix_of_twolevel (TL_T k i j) = twolevel (1,0) (0,omega^(k `mod` 8)) i j
matrix_of_twolevel (TL_omega k i) = onelevel (omega^(k `mod` 8)) i

-- | Convert a list of symbolic one- or two-level gates into an
-- operator. Note that the gates are to be executed right-to-left,
-- exactly as in mathematical notation.
matrix_of_twolevels :: (ComplexRing a, RootHalfRing a, Nat n) => [TwoLevel] -> Matrix n n a
matrix_of_twolevels gs = product [ matrix_of_twolevel g | g <- gs ]

-- ----------------------------------------------------------------------
-- ** Auxiliary functions

-- | Given an element of the form ω[sup /l/], return /l/∈{0,…,7}, or
-- 'Nothing' if not of that form.
log_omega :: ZOmega -> Maybe Integer
log_omega (Omega 0 0 0 1) = Just 0
log_omega (Omega 0 0 1 0) = Just 1
log_omega (Omega 0 1 0 0) = Just 2
log_omega (Omega 1 0 0 0) = Just 3
log_omega (Omega 0 0 0 (-1)) = Just 4
log_omega (Omega 0 0 (-1) 0) = Just 5
log_omega (Omega 0 (-1) 0 0) = Just 6
log_omega (Omega (-1) 0 0 0) = Just 7
log_omega _ = Nothing

-- | Given two residues /a/ and /b/, find an index /l/ such that /a/ +
-- ω[sup /l/]/b/ is 0000. If no such index exists, find an index /l/
-- such that /a/ + ω[sup /l/]/b/ is 1111. If no such index exists,
-- throw an error.
residue_offset :: Omega Z2 -> Omega Z2 -> Integer
residue_offset a b
  | (a + b) == 0 = 0
  | (a + omega * b) == 0 = 1
  | (a + omega^2 * b) == 0 = 2
  | (a + omega^3 * b) == 0 = 3
  | (a + b) == w1111 = 0
  | (a + omega * b) == w1111 = 1
  | (a + omega^2 * b) == w1111 = 2
  | (a + omega^3 * b) == w1111 = 3
  | otherwise = error $ "offset: no such offset"
    where
      w1111 = Omega 1 1 1 1

-- | Split a list into pairs. Return a list of pairs, and a final
-- element if the length of the list was odd.
list_pairs :: [a] -> ([(a,a)], [a])
list_pairs [] = ([], [])
list_pairs [h] = ([], [h])
list_pairs (h:k:t) = ((h,k):t',r') where (t',r') = list_pairs t

-- ----------------------------------------------------------------------
-- ** Synthesis

-- | Row reduction: Given a unit column vector /v/, generate a
-- sequence of two-level operators that reduces the /i/th standard
-- basis vector /e/[sub /i/] to /v/. Any rows that are already 0 in
-- both vectors are guaranteed not to be touched.
reduce_column :: (Nat n) => Matrix n One (DOmega) -> Index -> [TwoLevel]
reduce_column v i
  | k == 0 = 
    let 
      j = case findIndices (/= 0) w of
        [j] -> j
        _ -> error "reduce_column: not a unit vector"
      wj = w !! j
      l = case log_omega wj of
        Just l -> l
        Nothing -> error "reduce_column: not a unit vector"
      m1 = [TL_omega l j]
      m2 = if i==j then [] else [TL_X i j]
    in m1 ++ m2
  | otherwise = 
    let 
      res = residue w
      idx_res = zip3 [0..] res w
      res1010 = [ (i,a,x) | (i,a,x) <- idx_res, a * adj a == Omega 1 0 1 0 ]
      res0001 = [ (i,a,x) | (i,a,x) <- idx_res, a * adj a == Omega 0 0 0 1 ]
      res1010_pairs = case list_pairs res1010 of
        (p, []) -> p
        _ -> error "reduce_column: not a unit vector"
      res0001_pairs = case list_pairs res0001 of
        (p, []) -> p
        _ -> error "reduce_column: not a unit vector"
      m1010 = concat $ map lemma41 res1010_pairs
      m0001 = concat $ map lemma41 res0001_pairs
      lemma41 ((i,a,x), (j,b,y)) = [TL_T (-offs) i j, TL_H i j]
        where
          offs = residue_offset a b
      gates = m1010 ++ m0001
      v' = adj (matrix_of_twolevels gates) .*. v
    in gates ++ reduce_column v' i
  where
    (w, k) = denomexp_decompose (list_of_vector (vector_head (unMatrix v)))

-- | Input an exact /n/×/n/ unitary operator with coefficients in
-- /D/[ω], and output an equivalent sequence of two-level gates.  This
-- is the algorithm from the Giles-Selinger paper. It has
-- superexponential complexity.
synthesis_nqubit :: (Nat n) => Matrix n n DOmega -> [TwoLevel]
synthesis_nqubit m = aux (unMatrix m) 0 where
  aux :: (Nat m) => Vector n (Vector m DOmega) -> Index -> [TwoLevel]
  aux Nil i = []
  aux (c `Cons` cs) i = gates ++ aux (unMatrix m') (i+1)
    where
      gates = reduce_column (column_matrix c) i
      gates_matrix = matrix_of_twolevels gates
      m' = (adj gates_matrix) .*. (Matrix cs)
  
-- ----------------------------------------------------------------------
-- ** Some tests
  
-- | Some 3×3 unitary matrix.
m :: Matrix Three Three DOmega
m = matrix_of_twolevels [TL_H 0 1,TL_T 1 0 1, TL_H 1 2, TL_T 1 1 2, TL_H 0 2, TL_T 1 0 2]

-- | The first column of /m/[sup 10].
c = column_matrix (vector_head $ unMatrix (m^10))

-- | Gates for /c/.
gates = reduce_column c 0

-- | Matrix for 'gates'. Note: the first column of /m1/ is /c/.
m1 :: Matrix Three Three DOmega
m1 = matrix_of_twolevels gates

-- | Gates for /m/.
gates2 = synthesis_nqubit m

-- | Matrix for 'gates2'.
m2 :: Matrix Three Three DOmega
m2 = matrix_of_twolevels gates2

-- | Gates for /m/[sup 5].
gates3 = synthesis_nqubit (m^5)

-- | Matrix for 'gates3'.
m3 :: Matrix Three Three DOmega
m3 = matrix_of_twolevels gates3

-- ----------------------------------------------------------------------
-- * Random matrices over /D/[ω]

-- $ The following functions, and in particular
-- 'random_matrix_denexp', are used for generating (relatively) random
-- matrices of a given denominator exponent. 
-- 
-- Note: the matrices are not really uniformly distributed among all
-- those with equal denominator exponent. The selection is biased
-- toward matrices that are more likely to exhibit \"worst-case\"
-- behavior with respect to synthesis, i.e., they use the maximal
-- number of parallel /H/-operators at each step. 

-- ### Todo: change these functions to generate a symbolic
-- representation (a list of two-level gates), and only convert to
-- a matrix at the end.

-- | Generate a random permutation.
random_perm :: (Nat n, Num a, RandomGen g) => g -> Index -> Matrix n n a
random_perm g n = matrix_of_permutation phi where
  phi = random_permutation g n

-- | Given a permutation, expressed as a list of integers, return a
-- matrix representing the same permutation of basis vectors.
matrix_of_permutation :: (Nat n, Num a) => [Index] -> Matrix n n a
matrix_of_permutation p = matrix_of_function f where
  f i j
    | p !! i == j = 1
    | otherwise = 0
                
-- | Return a random permutation of length /n/.                  
random_permutation :: RandomGen g => g -> Index -> [Index]
random_permutation g 0 = []
random_permutation g n = take i p' ++ [n-1] ++ drop i p' where
  p' = random_permutation g1 (n-1)
  (i,g1) = randomR (0,n-1) g

-- | Generate a random phase shift.
random_phase :: (Nat n, OmegaRing a, RandomGen g) => g -> Index -> Matrix n n a
random_phase g n = matrix_of_function f where
  f i j
    | i == j = omega ^ (lst !! i)
    | otherwise = 0
  lst = take n $ randomRs (0,7::Index) g

-- | Apply a random number of two-level Hadamards.
random_diffuse :: (Nat n, ComplexRing a, RootHalfRing a, RandomGen g) => g -> Index -> Matrix n n a
random_diffuse g n = matrix_of_twolevels gates where
  (m,_) = randomR (0, n) g
  gates = [ TL_H i (i+1) | i <- [0,2..m-2] ]

-- | Apply the maximum number of two-level Hadamards.
matrix_diffuse :: (Nat n, ComplexRing a, RootHalfRing a) => Index -> Matrix n n a
matrix_diffuse n = matrix_of_twolevels gates where
  gates = [ TL_H i (i+1) | i <- [0,2..n-2] ]

-- | Generate a random matrix of denominator exponent /k/.
random_matrix_denexp :: (ComplexRing a, RootHalfRing a, Nat n, RandomGen g) => g -> Index -> Integer -> Matrix n n a
random_matrix_denexp g n 0 = perm * phase where
  perm = random_perm g1 n
  phase = random_phase g2 n
  (g1,g2) = split g
random_matrix_denexp g n k = diffuse * perm * phase * random_matrix_denexp g4 n (k-1) where
  (g1,g1') = split g
  (g2,g2') = split g1'
  (g3,g3') = split g2'
  (g4,g4') = split g3'
  perm = random_perm g1 n
  phase = random_phase g2 n
  diffuse = matrix_diffuse n
  
-- ----------------------------------------------------------------------
