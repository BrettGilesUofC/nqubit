module Main where
  import Programs.Synthesis.Synthesis
  import System.Random
  main = putStrLn "Not yet implemented."

  first_col :: (Nat n, Nat m) => Matrix m (Succ n) a -> Matrix m One a
  first_col (Matrix vs) = Matrix (Cons (vector_head vs) Nil)

  second_col :: (Nat n, Nat m) => Matrix m (Succ(Succ n)) a -> Matrix m One a
  second_col (Matrix vs) = Matrix (Cons (vector_head $ vector_tail vs) Nil)

  third_col :: (Nat n, Nat m) => Matrix m (Succ(Succ(Succ n))) a -> Matrix m One a
  third_col (Matrix vs) = Matrix (Cons (vector_head $ vector_tail $ vector_tail vs) Nil)

  fourth_col :: (Nat n, Nat m) => Matrix m (Succ(Succ(Succ(Succ n)))) a -> Matrix m One a
  fourth_col (Matrix vs) = Matrix (Cons (vector_head $ vector_tail $ vector_tail $ vector_tail vs) Nil)

  ex :: Matrix Four Four DOmega
  ex = ((controlled u2_S) * (controlled u2_H) * (tensor u2_T u2_H) * (controlled u2_T) *
    (tensor u2_H u2_H) * (cnot)*(controlled u2_H) * (controlled u2_T) * (tensor u2_H u2_X))

  ex11 = head $  matrix_entries $ second_col ex
  ex12 = head $  tail $ matrix_entries $ second_col ex

  Omega a11 b11 c11 d11 = ex11
  Omega a12 b12 c12 d12 = ex12


  rho_domega :: DOmega -> Omega Z2
  rho_domega o = residue (Omega a' b' c' d')
    where
      (Omega a' b' c' d',_) = denomexp_decompose o
      -- a'' = fromInteger a'
      --   b'' = fromInteger b'
      --   c'' = fromInteger c'
      --   d'' = fromInteger d'

  rho_domega_k :: Integer -> DOmega -> Omega Z2
  rho_domega_k k o =
    if (k >= j)
      then if even diff
             then residue (2^(diff `quot` 2) * Omega a' b' c' d')
             else residue (roottwo * 2^(diff `quot` 2) * Omega a' b' c' d')
      else error $ "denom exp of "++ show o ++ ", "++ show j ++ " is > input, "++show k++"."
    where
      (Omega a' b' c' d',j) = denomexp_decompose o
      diff = k-j
      -- factor =  if even (k-j)
      --           then 2 ^ ((k-j) `quot` 2)
      --           else roottwo * 2 ^ ((k-j) `quot` 2)
      -- Omega a' b' c' d' = oabcd * factor
      -- a'' = fromInteger a'
      --     b'' = fromInteger b'
      --     c'' = fromInteger c'
      --     d'' = fromInteger d'

  rho_mat ::(Nat n, Nat m)=> Matrix n m DOmega -> Matrix n m (Omega Z2)
  rho_mat  mat = matrix_map (rho_domega_k j) mat
      where (_,j) = denomexp_decompose mat

  rho_mat_k ::(Nat n, Nat m)=> Integer -> Matrix n m DOmega -> Matrix n m (Omega Z2)
  rho_mat_k  j mat = matrix_map (rho_domega_k j) mat

-- extras for manually trying it





  fc = first_col ex
  tlh01 = matrix_of_twolevel (TL_H 0 1) :: Matrix Four Four DOmega
  tlh02 = matrix_of_twolevel (TL_H 0 2) :: Matrix Four Four DOmega
  tlh03 = matrix_of_twolevel (TL_H 0 3) :: Matrix Four Four DOmega
  tlh12 = matrix_of_twolevel (TL_H 1 2) :: Matrix Four Four DOmega
  tlh13 = matrix_of_twolevel (TL_H 1 3) :: Matrix Four Four DOmega
  tlh23 = matrix_of_twolevel (TL_H 2 3) :: Matrix Four Four DOmega


  tlx01 = matrix_of_twolevel (TL_X 0 1) :: Matrix Four Four DOmega
  tlx02 = matrix_of_twolevel (TL_X 0 2) :: Matrix Four Four DOmega
  tlx03 = matrix_of_twolevel (TL_X 0 3) :: Matrix Four Four DOmega
  tlx12 = matrix_of_twolevel (TL_X 1 2) :: Matrix Four Four DOmega
  tlx13 = matrix_of_twolevel (TL_X 1 3) :: Matrix Four Four DOmega
  tlx23 = matrix_of_twolevel (TL_X 2 3) :: Matrix Four Four DOmega

  tlt01 = \n-> matrix_of_twolevel (TL_T n 0 1) :: Matrix Four Four DOmega
  tlt02 = \n-> matrix_of_twolevel (TL_T n 0 2) :: Matrix Four Four DOmega
  tlt03 = \n-> matrix_of_twolevel (TL_T n 0 3) :: Matrix Four Four DOmega
  tlt12 = \n-> matrix_of_twolevel (TL_T n 1 2) :: Matrix Four Four DOmega
  tlt13 = \n-> matrix_of_twolevel (TL_T n 1 3) :: Matrix Four Four DOmega
  tlt23 = \n-> matrix_of_twolevel (TL_T n 2 3) :: Matrix Four Four DOmega


  om0 = onelevel omega 0 :: Matrix Four Four DOmega
  om1 = onelevel omega 1 :: Matrix Four Four DOmega
  om2 = onelevel omega 2 :: Matrix Four Four DOmega
  om3 = onelevel omega 3 :: Matrix Four Four DOmega

  c1Step1 = tlh02
  c1Step2 = tlh13 * om3
  c1Step3 = tlh23
  c1Step4 = tlh13 * om1
  c1Step5 = tlx03 * om3 * om3 * om3 * om3 * om3 * om3 * om3

  c1all :: Matrix Four Four DOmega
  c1all = c1Step5 * c1Step4 * c1Step3 * c1Step2 * c1Step1

  sc = second_col $ c1all * ex
  c2s1 = tlh23 * om3
  c2s2 = tlh13
  c2s3 = tlh12 * om2
  c2s4 = tlh12 * om1
  c2s5 = tlx12 * om2 * om2 * om2 * om2 * om2 * om2
  c2all = c2s5 * c2s4 * c2s3 * c2s2 * c2s1
  c3all = tlx23 * om3 * om3 * om3 * om3 * om3 * om3
  c4all = om3 * om3

  decomposer =
    c4all * c3all * c2all * c1all

  exa = adj ex
  fca = first_col exa

  ca1S1a = tlh01 * om1
  ca1S1b = tlh01 * om1

  ca1S2a = tlh23 * om3
  ca1S2b = tlh23 * om3

  ca1S3 = tlh12

  ca1S4 = tlh12 * om2 * om2 * om2
  ca1S5 = tlx02 * om2 * om2 * om2 * om2 *om2 *om2 * om2
  ca1all = ca1S3 * ca1S2b * ca1S2a * ca1S1b * ca1S1a

  decomp_a = ca1all

  nex = (ca1S1b * ca1S1a) * (adj ex)

  cn1S1a = tlh23 * om3
  cn1S1b = tlh23 * om3

  cn1S2 = tlh12

  cn1S3 = tlh12 * om2 * om2 * om2
  cn1S4 = tlx02 * om2 * om2 * om2 * om2 *om2 *om2 * om2
  cn1all = cn1S4 * cn1S3 * cn1S2 * cn1S1b * cn1S1a

  cn2S1a = tlh23
  cn2S1b = tlh23 * om2
  cn2S2 = tlh12 * om1
  cn2S3 = tlh12 * om2
  cn2S4 = tlx12 * om2 * om2 * om2 * om2 * om2 * om2 * om2
  cn2all = cn2S4 * cn2S3 * cn2S2 * cn2S1b * cn2S1a

  cn3S1 = tlh23 * om2
  cn3S2 = om2 * om2 * om2 * om2 * om2 * om2 * om2
  cn3all = cn3S2 * cn3S1

  cn4all = om3 * om3 * om3 * om3 * om3 * om3
  decomp_n = cn4all * cn3all * cn2all * cn1all

  e1 ::Matrix Four One DOmega
  e1 = column_matrix (Cons 1 (Cons 0 (Cons 0 (Cons 0 Nil))))

  v_sqnm :: (Nat n)=> Vector n DOmega -> DOmega
  v_sqnm = (vector_foldl (+) 0) . (vector_map (\x-> x*(adj x)))

  osq = omega*omega
  ocu = omega*osq

  uncol :: Matrix Four One DOmega -> Vector Four DOmega
  uncol = vector_head . unMatrix

  mo_sqnm = v_sqnm . uncol

  trial :: Matrix Four One DOmega
  trial = scalarmult (roothalf * roothalf * roothalf)
                    $ m4x1 (ocu+osq+1, 1,omega+1,osq-omega)

  t2 = scalarmult (roothalf * roothalf * roothalf)
                    $ m4x1 (1,0,ocu+omega,osq+2)

  tS1 = tlh23 * om2
  tS2 = tlh01*om1*tlh01 * om0
  tS3 = tlh23*om3
  tS4 = tlh01*om0
  tS5 = tlh02
  tS6=  om0*om0*om0*om0*om0*om0
  t1all = tS6*tS5*tS4*tS3* tS2* tS1

  t1allrev = om2*om2*om2*om2*om2*om2*om2 * tlh23 *
              om0*om0*om0*om0*om0*om0*om0* tlh01 *
              om1*om1*om1*om1*om1*om1*om1* tlh01 *
              om3*om3*om3*om3*om3*om3*om3* tlh23 *
              om0*om0*om0*om0*om0*om0*om0* tlh01 *
              tlh02 *
              om0*om0

  m4x1 :: (a, a, a, a) ->  Matrix Four One a
  m4x1 (a0, a1, a2, a3)  = Matrix ((a0 `Cons` a1 `Cons` a2 `Cons` a3 `Cons` Nil)
                           `Cons` Nil)
-- Modify to add something in t1allrev to get col1 lde 4,
-- need to chose so cols 3,4 do not increase.

  new_ex = t1allrev .*. (cn2all * cn1all * nex)

  tex = om1*om1 * om3 *nex * tlx03

  fcx = first_col tex

  x1S1 = tlh23 * om3
  x1S2a = tlh01 * om1 * om1
  x1S2b = tlh01 * om1
  x1S3  = tlh01 * om1
  x1S4 = tlh03 * om0
  x1S5 = tlh23 * om2
  x1S6 = tlx03
  x1all = x1S6 * x1S5 * x1S4 * x1S3 * x1S2b * x1S2a * x1S1

  x2AndRest = om1*om1*om1*om1*om1*tlh13 * om3 * om3 * om3 *tlh12*om1*tlh12 * tlh13* om3*om3

  fa = first_col aex
  aex = tlh23*om2*ex*tlx03

  a1all = om0 * om0 * om0 * tlx02 *
            tlh12 * om2 *
            tlh12 * om2 * om2 *
            tlh01 * om1 * tlh01 * om1 *om1 *
            tlh23 * om3

  sa = second_col (a1all * aex)

  a2all = om1*om1*om1*om1*om1*om1*om1*tlx12*
            tlh23 * om2 *
            tlh23 * om3 * om3

  ta = third_col (a2all * a1all * aex)

  a3all = om2 *om2 *om2 *om2 * tlx23 *
            tlh23 * om2 *
            tlh23

  a4all = om3 *om3 *om3 *om3

  bex = tlx13 * aex * om1 * om2

  fb = first_col bex

  b1all = om0 * om0 * om0 * om0 * om0 * om0 *om0 * tlx03 *
            tlh23 * (tlt23 3) *
            tlh23 * (tlt23 1)  *
            tlh03 * (tlt03 1) * tlh03 * (tlt03 2) *
            tlh12 * (tlt12 3)

  sb = second_col (b1all * bex)

  b2all = om1*om1*om1*om1*om1*om1*
            tlh13 * (tlt13 3) *
            tlh13 * (tlt13 1)

  tb = third_col (b2all * b1all * bex)

  b3all = om2 *om2 *om2 *om2 *
            tlh23 * (tlt23 3) *
            tlh23

  b4all = om3 *om3 *om3 *om3 * om3


  nth_col n mat = column_matrix (head $ drop (n-1) $ list_of_vector $ unMatrix mat)

  check_mat m =
    let c1 = nth_col 1 m
        c1_twolevels = reduce_column c1 0
        c1_mat = adj $ matrix_of_twolevels c1_twolevels
    in c1_mat * m

  g = read "94954873 40691" :: StdGen

  m4_k g k = random_matrix_denexp g 4 k :: Matrix Four Four DOmega

  e20 = ex ^ 20

  ec1 = nth_col 1 e20
  rec1 = reduce_column ec1 1
  e20' = (adj $ matrix_of_twolevels rec1) * e20
  rec2 = reduce_column (nth_col 2 e20') 2









