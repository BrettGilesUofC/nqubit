-- try for big worst case stuff

  g = read "94954873 40691" :: StdGen

  m13_7 = random_matrix_denexp g 13 7 :: Matrix (Succ (Succ (Succ Ten))) (Succ (Succ (Succ Ten))) DOmega

  nth_col n mat = column_matrix (head $ drop (n-1) $ list_of_vector $ unMatrix mat)



  m13_7_c1 = nth_col 1 m13_7
  c1_twolevels = reduce_column m13_7_c1 1
  c1_mat = matrix_of_twolevels c1_twolevels

  m13_7_r1 = c1_mat * m13_7

  m13_7_c2 = nth_col 2 m13_7_r1
  c2_twolevels = reduce_column m13_7_c2 2
  c2_mat = matrix_of_twolevels c2_twolevels

  m13_7_r2 = c2_mat * m13_7_r1

  m13_7_c3 = nth_col 3 m13_7_r2
  c3_twolevels = reduce_column m13_7_c3 3
  c3_mat = matrix_of_twolevels c3_twolevels

  m13_7_r3 = c3_mat * m13_7_r2

  m13_7_c4 = nth_col 1 m13_7_r3
  c4_twolevels = reduce_column m13_7_c4 4
  c4_mat = matrix_of_twolevels c4_twolevels

  m13_7_r4 = c3_mat * m13_7_r3

  m13_7_c5 = nth_col 1 m13_7_r4
  c5_twolevels = reduce_column m13_7_c5 5
  c5_mat = matrix_of_twolevels c5_twolevels

  m13_7_r5 = c4_mat * m13_7_r4

  m13_7_c6 = nth_col 1 m13_7_r5
  c6_twolevels = reduce_column m13_7_c6 6
  c6_mat = matrix_of_twolevels c6_twolevels

  m13_7_r6 = c5_mat * m13_7_r5

  m13_7_c7 = nth_col 1 m13_7_r6
  c7_twolevels = reduce_column m13_7_c7 7
  c7_mat = matrix_of_twolevels c7_twolevels

  m13_7_r7 = c6_mat * m13_7_r6

  m13_7_c8 = nth_col 1 m13_7_r7
  c8_twolevels = reduce_column m13_7_c8 8
  c8_mat = matrix_of_twolevels c8_twolevels

  m13_7_r8 = c7_mat * m13_7_r7

  m13_7_c9 = nth_col 1 m13_7_r8
  c9_twolevels = reduce_column m13_7_c9 9
  c9_mat = matrix_of_twolevels c9_twolevels

  m13_7_r9 = c8_mat * m13_7_r8

  m13_7_c10 = nth_col 1 m13_7_r9
  c10_twolevels = reduce_column m13_7_c10 10
  c10_mat = matrix_of_twolevels c10_twolevels

  m13_7_r10 = c9_mat * m13_7_r9

  m13_7_c11 = nth_col 1 m13_7_r10
  c11_twolevels = reduce_column m13_7_c11 11
  c11_mat = matrix_of_twolevels c11_twolevels

  m13_7_r11 = c10_mat * m13_7_r10

  m13_7_c12 = nth_col 1 m13_7_r11
  c12_twolevels = reduce_column m13_7_c12 12
  c12_mat = matrix_of_twolevels c12_twolevels

  m13_7_r12 = c11_mat * m13_7_r11

  m13_7_c13 = nth_col 1 m13_7_r12
  c13_twolevels = reduce_column m13_7_c13 13
  c13_mat = matrix_of_twolevels c13_twolevels :: Matrix (Succ (Succ (Succ Ten))) (Succ (Succ (Succ Ten))) DOmega

  m13_7_r13 = c12_mat * m13_7_r12

