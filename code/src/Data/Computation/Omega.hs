module Data.Computation.Omega  where

data OmegaRing a where
  ZOmega :: Num a => a -> a -> a -> a -> OmegaRing a