module Main where
import Test.Hspec.Monadic
--import Test.Hspec.QuickCheck
import Test.Hspec.Core(Example(..),Result(..))
import Test.Hspec.HUnit
--import Test.QuickCheck hiding (property)
import Test.HUnit
import Programs.Synthesis.Synthesis


omegaSq :: DOmega
omegaSq = omega * omega
omegaCu = omegaSq * omega
hr2Sq = roothalf * roothalf
hr2Cu = roothalf * roothalf * roothalf

main = hspec synthspecs
synthspecs =
  describe "DOmega" $ do
    context "Denominator Exp" $ do
      it "should give -1011,1 for denomexp of (- omegaCu + omega + 1) * hr2" $ do
        ( denomexp (roothalf * (-omegaCu + omega + 1))@?= (Omega (-1) 0 1 1, 1) )
      it "should give -1011,2 for denomexp of (- omegaCu + omega + 1) * hr2Sq" $ do
        ( denomexp (hr2Sq * (-omegaCu + omega + 1))@?= (Omega (-1) 0 1 1, 2) )
      it "should give -1011,3 for denomexp of (- omegaCu + omega + 1) * hr2cu" $ do
        ( denomexp (hr2Cu * (-omegaCu + omega + 1))@?= (Omega (-1) 0 1 1, 3) )
      it "should give -1011,4 for denomexp of (- omegaCu + omega + 1) * hr2^4" $ do
        ( denomexp (roothalf * hr2Cu * (-omegaCu + omega + 1))@?= (Omega (-1) 0 1 1, 4) )
      it "should give 0001,0 for denomexp of (- omegaCu + omega ) * hr2" $ do
        ( denomexp (roothalf * (-omegaCu + omega ))@?= (Omega 0 0 0 1, 0) )
      it "should give 0001,1 for denomexp of (- omegaCu + omega)  * hr2Sq" $ do
        ( denomexp (hr2Sq * (-omegaCu + omega ))@?= (Omega 0 0 0 1, 1) )
      it "should give 0001,2 for denomexp of (- omegaCu + omega)  * hr2cu" $ do
        ( denomexp (hr2Cu * (-omegaCu + omega ))@?= (Omega 0 0 0 1, 2) )
      it "should give 0001,3 for denomexp of (- omegaCu + omega)  * hr2^4" $ do
        ( denomexp (roothalf * hr2Cu * (-omegaCu + omega ))@?= (Omega 0 0 0 1, 3) )